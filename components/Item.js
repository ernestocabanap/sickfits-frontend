import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link'; 
import Title from './styles/Title';
import ItemStyle from './styles/ItemStyles';
import PriceTag from './styles/PriceTag';
import formatMoney from '../lib/formatMoney';
import DeleteItem from './DeleteItem';
import AddToCart from './AddToCart';

class Item extends Component {
  static propTypes = {
        /*Chequear el video React Meets GraphQL 16:14 y verificar que hay que aprender PropTypes
      En el libro pagina 47
      item: PropTypes.shape({
        title: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
      })

    */
   item: PropTypes.object.isRequired,
  }

  render() {
    const { item } = this.props;
    return (
      <ItemStyle>
          {item.image && <img src={item.image} alt={item.title} />}
          {/* The same as the ternary operator */}
          <Title>
            <Link href={{
                pathname: '/item',
                query: { id: item.id },
            }}>
                <a>{item.title}</a>
            </Link>
            </Title>
            <PriceTag>
                {formatMoney(item.price)}
            </PriceTag>
            <p>{item.description}</p>
            
            <div className="buttonList">
                <Link href={{
                    pathname: 'update',
                    query: { id: item.id },
                }}>
                    <a>Edit</a>
                </Link>
                <AddToCart id={item.id}/>
                <DeleteItem id={item.id}>Delete this item</DeleteItem>
            </div>    
      </ItemStyle>
    );
  }
}

export default Item;
